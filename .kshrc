#kshrc openbsd JUNE 2020

export EDITOR=nvim

#path
export PATH="$PATH:$HOME/bin"

#go path
export GOPATH="$HOME/go"
export PATH="$PATH:$GOPATH/bin"

HISTFILE=$HOME/.ksh_history
HISTSIZE=20000

## Reset to normal: \033[0m
NORM="\033[0m"

## Colors:
RED="\033[0;31m"
CYN="\033[36m"

#prompt
PS1="\n${CYN}[ \w ]\n${RED}▋  ${NORM}"

set -o emacs
alias __A=$(print '\0020') # ^P = up = previous command
alias __B=$(print '\0016') # ^N = down = next command
alias __C=$(print '\0006') # ^F = right = forward a character
alias __D=$(print '\0002') # ^B = left = back a character
alias __H=$(print '\0001') # ^A = home = beginning of line

alias c="clear"
alias ls="colorls -aGp"
alias n="nvim"
alias rm="rm -i"

mkd() {
  mkdir -p "$1" && cd "$1"
}
