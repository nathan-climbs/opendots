"simple status
function! ReadOnly()
  if &readonly || !&modifiable
    return 'R '
  else
    return ''
endfunction

function! PasteSpell()
  let paste = &paste ? ' P ' : ''
  let spell = &spell ? ' SPL ' : ''
  return paste . spell
endfunction

function! LightFilename()
  let filename = expand('%:t') !=# '' ? expand('%:t') : 'none'
  let modified = &modified ? ' ● ' : '  '
  return filename . modified
endfunction

hi statusline ctermbg=238 cterm=italic gui=italic guibg=#333333

set laststatus=2
set statusline=%!CreateStatusline()
augroup status
    autocmd!
    autocmd WinEnter * setlocal statusline=%!CreateStatusline()
    autocmd WinLeave * setlocal statusline=%!CreateInactiveStatusline()
augroup END

function! CreateStatusline()
  let statusline=''
  let statusline.='%#statusline#'
  let statusline.='  %{LightFilename()} '
  let statusline.=' %{ReadOnly()}'
  let statusline.=' %{PasteSpell()}'
  let statusline.=" %{coc#status()}%{get(b:,'coc_current_function','')}"
  let statusline.='%='
  let statusline.='%l:%c '
  let statusline.=' %P  '
  return statusline
endfunction

function! CreateInactiveStatusline()
  let statusline=''
  let statusline.=' %t'
  let statusline.='%='
  let statusline.=' %P  '
  return statusline
endfunction
